
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/16/2018 09:08:01
-- Generated from EDMX file: E:\TestJob\TestJob\Model\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Database];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_GroupStudent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudentSet] DROP CONSTRAINT [FK_GroupStudent];
GO
IF OBJECT_ID(N'[dbo].[FK_FacultyGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupSet] DROP CONSTRAINT [FK_FacultyGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_StudySubjectStudyPlanWorks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudyPlanWorksSet] DROP CONSTRAINT [FK_StudySubjectStudyPlanWorks];
GO
IF OBJECT_ID(N'[dbo].[FK_StudyPlanWorksGradesList]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GradesListSet] DROP CONSTRAINT [FK_StudyPlanWorksGradesList];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentGradesList]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GradesListSet] DROP CONSTRAINT [FK_StudentGradesList];
GO
IF OBJECT_ID(N'[dbo].[FK_SemesterStudyPlanWorks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudyPlanWorksSet] DROP CONSTRAINT [FK_SemesterStudyPlanWorks];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupStudyPlanWorks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudyPlanWorksSet] DROP CONSTRAINT [FK_GroupStudyPlanWorks];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[StudentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StudentSet];
GO
IF OBJECT_ID(N'[dbo].[FacultySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FacultySet];
GO
IF OBJECT_ID(N'[dbo].[GroupSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupSet];
GO
IF OBJECT_ID(N'[dbo].[SemesterSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SemesterSet];
GO
IF OBJECT_ID(N'[dbo].[StudyPlanWorksSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StudyPlanWorksSet];
GO
IF OBJECT_ID(N'[dbo].[StudySubjectSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StudySubjectSet];
GO
IF OBJECT_ID(N'[dbo].[GradesListSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GradesListSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'StudentSet'
CREATE TABLE [dbo].[StudentSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [MiddleName] nvarchar(max)  NOT NULL,
    [SecondName] nvarchar(max)  NOT NULL,
    [GroupId] int  NOT NULL,
    [StatusId] int  NOT NULL
);
GO

-- Creating table 'FacultySet'
CREATE TABLE [dbo].[FacultySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ShortName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GroupSet'
CREATE TABLE [dbo].[GroupSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FacultyId] int  NOT NULL,
    [Year] int  NOT NULL,
    [Number] int  NOT NULL
);
GO

-- Creating table 'SemesterSet'
CREATE TABLE [dbo].[SemesterSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Year] int  NOT NULL,
    [Number] int  NOT NULL
);
GO

-- Creating table 'StudyPlanWorksSet'
CREATE TABLE [dbo].[StudyPlanWorksSet] (
    [SemesterId] int  NOT NULL,
    [GroupId] int  NOT NULL,
    [SubjectId] int  NOT NULL,
    [Work] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'StudySubjectSet'
CREATE TABLE [dbo].[StudySubjectSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ShortName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GradesListSet'
CREATE TABLE [dbo].[GradesListSet] (
    [StudentId] int  NOT NULL,
    [PlanId] int  NOT NULL,
    [Mark] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'StudentSet'
ALTER TABLE [dbo].[StudentSet]
ADD CONSTRAINT [PK_StudentSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FacultySet'
ALTER TABLE [dbo].[FacultySet]
ADD CONSTRAINT [PK_FacultySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GroupSet'
ALTER TABLE [dbo].[GroupSet]
ADD CONSTRAINT [PK_GroupSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SemesterSet'
ALTER TABLE [dbo].[SemesterSet]
ADD CONSTRAINT [PK_SemesterSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'StudyPlanWorksSet'
ALTER TABLE [dbo].[StudyPlanWorksSet]
ADD CONSTRAINT [PK_StudyPlanWorksSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'StudySubjectSet'
ALTER TABLE [dbo].[StudySubjectSet]
ADD CONSTRAINT [PK_StudySubjectSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [StudentId], [PlanId] in table 'GradesListSet'
ALTER TABLE [dbo].[GradesListSet]
ADD CONSTRAINT [PK_GradesListSet]
    PRIMARY KEY CLUSTERED ([StudentId], [PlanId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [GroupId] in table 'StudentSet'
ALTER TABLE [dbo].[StudentSet]
ADD CONSTRAINT [FK_GroupStudent]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[GroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupStudent'
CREATE INDEX [IX_FK_GroupStudent]
ON [dbo].[StudentSet]
    ([GroupId]);
GO

-- Creating foreign key on [FacultyId] in table 'GroupSet'
ALTER TABLE [dbo].[GroupSet]
ADD CONSTRAINT [FK_FacultyGroup]
    FOREIGN KEY ([FacultyId])
    REFERENCES [dbo].[FacultySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FacultyGroup'
CREATE INDEX [IX_FK_FacultyGroup]
ON [dbo].[GroupSet]
    ([FacultyId]);
GO

-- Creating foreign key on [SubjectId] in table 'StudyPlanWorksSet'
ALTER TABLE [dbo].[StudyPlanWorksSet]
ADD CONSTRAINT [FK_StudySubjectStudyPlanWorks]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[StudySubjectSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudySubjectStudyPlanWorks'
CREATE INDEX [IX_FK_StudySubjectStudyPlanWorks]
ON [dbo].[StudyPlanWorksSet]
    ([SubjectId]);
GO

-- Creating foreign key on [PlanId] in table 'GradesListSet'
ALTER TABLE [dbo].[GradesListSet]
ADD CONSTRAINT [FK_StudyPlanWorksGradesList]
    FOREIGN KEY ([PlanId])
    REFERENCES [dbo].[StudyPlanWorksSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudyPlanWorksGradesList'
CREATE INDEX [IX_FK_StudyPlanWorksGradesList]
ON [dbo].[GradesListSet]
    ([PlanId]);
GO

-- Creating foreign key on [StudentId] in table 'GradesListSet'
ALTER TABLE [dbo].[GradesListSet]
ADD CONSTRAINT [FK_StudentGradesList]
    FOREIGN KEY ([StudentId])
    REFERENCES [dbo].[StudentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [SemesterId] in table 'StudyPlanWorksSet'
ALTER TABLE [dbo].[StudyPlanWorksSet]
ADD CONSTRAINT [FK_SemesterStudyPlanWorks]
    FOREIGN KEY ([SemesterId])
    REFERENCES [dbo].[SemesterSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SemesterStudyPlanWorks'
CREATE INDEX [IX_FK_SemesterStudyPlanWorks]
ON [dbo].[StudyPlanWorksSet]
    ([SemesterId]);
GO

-- Creating foreign key on [GroupId] in table 'StudyPlanWorksSet'
ALTER TABLE [dbo].[StudyPlanWorksSet]
ADD CONSTRAINT [FK_GroupStudyPlanWorks]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[GroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupStudyPlanWorks'
CREATE INDEX [IX_FK_GroupStudyPlanWorks]
ON [dbo].[StudyPlanWorksSet]
    ([GroupId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------