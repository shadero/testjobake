﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestJob.Model
{
    class Singleton<T> where T : new()
    {
        static T instance;
        static public T Instance
        {
            get
            {
                if (instance == null) instance = new T();
                return instance;
            }
        }
    }

    class DataModel : Singleton<DataModelContainer>
    {   }
}
