﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestJob.Model
{
    partial class Group
    {
        public string Name
        {
            get
            {
                return Faculty?.ShortName + "-" + this.Year.ToString("00") + this.Number;
            }
        }
    }
}
