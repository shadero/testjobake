﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TestJob.Model
{

    public enum StudentStatus
    {
        Candidate = 0,
        Active = 1,
        Expelled = 2,
        Sick = 3,
        Academic = 4
    }

    public class StatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var ret = new List<string>();
            if (value == null) return null;

            foreach (var inp in (IEnumerable<StudentStatus>)value)
            {
                var val = "Неизвестный";

                switch ((StudentStatus)inp)
                {
                    case StudentStatus.Academic:
                        val = "Академический отпуск";
                        break;
                    case StudentStatus.Active:
                        val = "Активный";
                        break;
                    case StudentStatus.Candidate:
                        val = "Кандидат";
                        break;
                    case StudentStatus.Expelled:
                        val = "Отчислен";
                        break;
                    case StudentStatus.Sick:
                        val = "Больничный";
                        break;
                }
                ret.Add(val);
            }
            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;

            var ret = new List<StudentStatus>();

            foreach (var inp in (IEnumerable<string>)value)
            {
                var val = StudentStatus.Expelled;

                switch ((string)inp)
                {
                    case "Академический отпуск":
                        val = StudentStatus.Academic;
                        break;
                    case "Активный":
                        val = StudentStatus.Active;
                        break;
                    case "Кандидат":
                        val = StudentStatus.Candidate;
                        break;
                    case "Отчислен":
                        val = StudentStatus.Expelled;
                        break;
                    case "Больничный":
                        val = StudentStatus.Sick;
                        break;
                }

                ret.Add(val);
            }

            return ret;
        }
    }

    partial class Student
    {
        public StudentStatus Status
        {
            get
            {
                return (StudentStatus)StatusId;
            }
        }
    }
}
