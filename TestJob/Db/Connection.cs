﻿using System;
using System.Data;
using System.Data.Common;
using System.Runtime.InteropServices;

namespace TestJob.Db
{
    public static partial class Connection
    {
        public static IQueryLogger Logger { get; set; }

        static ConnectionCache cache = new ConnectionCache();

        public static int ExecutionTimeout { get; set; } = 0;
        public static QueryTyping QueryTyping { get; set; } = QueryTyping.Strong;

        public static DbConnection Default
        {
            get { return cache.GetDefault(); }
        }

        public static DbConnection Use(string name)
        {
            var ret = cache.Get(name);
            if (ret == null) throw new ArgumentOutOfRangeException();
            return ret;
        }

        public static DbConnection As(this DbConnection conn, string name)
        {
            cache.Set(conn, name);
            return conn;
        }

        public static DbConnection AsDefault(this DbConnection conn)
        {
            cache.MakeDefault(conn);
            return conn;
        }

        public static Query Query(this DbConnection connection, string query)
        {
            return new Query(connection, query);
        }

        public static Query<T> Query<T>(this DbConnection connection, string query)
        {
            return new Query<T>(connection, query);
        }

        public static Query Query(this DbConnection connection, string query, CommandType type, params DbParameter[] args)
        {
            return new Query(connection, query, type, args);
        }

        public static Query<T> Query<T>(this DbConnection connection, string query, CommandType type, params DbParameter[] args)
        {
            return new Query<T>(connection, query, type, args);
        }

#if DEBUG
        static class QueryConsoleExtension
        {
            [DllImport("kernel32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            static extern bool AllocConsole();

            public static void Open()
            {
                AllocConsole();
            }
        }

        class DebugLogger : IQueryLogger
        {
            static bool consoleOpened = false;
            public void Write(string query)
            {
                if (!consoleOpened) QueryConsoleExtension.Open();
                var ts = $"[{DateTime.Now.ToShortDateString()} {DateTime.Now.ToLongTimeString()}]\t";
                Console.WriteLine(ts + query);
            }
        }

        static Connection()
        {
            if (Logger == null) Logger = new DebugLogger();
        }
#endif

    }
}
