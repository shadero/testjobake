﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace TestJob.Db
{
    public partial class Query<T> : IQuery<T>
    {
        static IQueryLogger Logger { get { return Connection.Logger; } }
        readonly string query;

        readonly DbConnection connection;
        readonly CommandType type;
        readonly DbParameter[] args;


        public Query(DbConnection connection, string query, CommandType type = CommandType.Text, params DbParameter[] args)
        {
            this.connection = connection;
            this.query = query;
            this.type = type;
            this.args = args;
        }

        DbConnection RestoreConnection()
        {
            if (connection.State == ConnectionState.Broken)
                connection.Close();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            return connection;
        }

        DbDataReader ExecuteCommand(int timeout)
        {
            //DbConnection conn = null;
            //DbCommand cmd = null;

            lock (connection)
            {
                var conn = RestoreConnection();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.CommandType = type;
                    cmd.CommandTimeout = (timeout == int.MaxValue ? 0 : timeout);

                    if (args != null) cmd.Parameters.AddRange(args);

                    Logger?.Write(query);

                    try
                    {
                        return cmd.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        Logger?.Write(ex.Message);
                        Logger?.Write(ex.StackTrace);
                        throw ex;
                    }
                }
            }
        }

        DbDataReader ExecuteReader(int? timeout)
        {
            Exception exception = null;

            var tout = timeout ?? Connection.ExecutionTimeout;
            
            var runTime = Environment.TickCount;

            while (true)
            {
                var elapsed = Environment.TickCount - runTime;
                var remain = tout - elapsed;

                if (tout != int.MaxValue && tout != 0 && remain <= 0)
                    break;

                try
                {
                    var cmdTimeOut = (tout == int.MaxValue || tout == 0) ? 0 : (int)remain;
                    return ExecuteCommand(cmdTimeOut);
                }
                catch (SqlException ex)
                {
                    if (!(connection.State == ConnectionState.Closed ||
                        connection.State == ConnectionState.Broken || ex.ErrorCode == -2146232060))
                        throw ex;

                    exception = ex;
                }
                catch (Exception ex)
                {
                    if (!(connection.State == ConnectionState.Closed || connection.State == ConnectionState.Broken))
                        throw ex;

                    exception = ex;
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();

                elapsed = Environment.TickCount - runTime;

                if (tout != 0 && (tout == int.MaxValue || elapsed < tout))
                {
                    Thread.Sleep(1);
                    continue;
                }
                break;
            }

            if (exception == null) throw new TimeoutException();
            throw exception;
        }
        
        IEnumerable<T> ExecuteEnumerable(QueryTyping typing, int? timeout)
        {
            lock (connection)
            {
                using (var reader = ExecuteReader(timeout))
                {
                    if (typeof(T) == typeof(object))
                        foreach (var r in ReadDynamic(reader, typing))
                            yield return r;
                    else
                        foreach (var r in ReadStrict(reader, typing))
                            yield return r;
                }
            }
        }

        Schema GetSchema(DbDataReader reader)
        {
            var count = reader.FieldCount;

            var scheme = new Schema();

            for (int i = 0; i < count; i++)
            {
                var key = reader.GetName(i).ToLower();

                if (!scheme.ContainsKey(key))
                    scheme.Add(key, new Schema.Field() { Ordinal = i, Type = reader.GetFieldType(i), Name = reader.GetName(i) });
            }

            return scheme;
        }

        public List<T> Execute(QueryTyping typing = QueryTyping.Inherits, int? timeout = null)
        {
            return ExecuteEnumerable(typing, timeout).ToList();
        }

        public List<T> TryExecute(QueryTyping typing = QueryTyping.Inherits, int? timeout = null)
        {
            Exception exc;
            return TryExecute(out exc, typing, timeout).ToList();
        }

        public List<T> TryExecute(out Exception exception, QueryTyping typing = QueryTyping.Inherits, int? timeout = null)
        {
            exception = null;

            try
            {
                return ExecuteEnumerable(typing, timeout).ToList();
            }
            catch (Exception e)
            {
                exception = e;
                return new List<T>();
            }
        }

        public Schema Extract(int? timeout = null)
        {
            using (var reader = ExecuteReader(timeout))
                return GetSchema(reader);
        }
    
        IEnumerable<T> ReadDynamic(DbDataReader reader, QueryTyping typing)
        {
            var count = reader.FieldCount;

            var schema = GetSchema(reader);

            while (reader.Read())
            {
                var values = new object[count];
                reader.GetValues(values);
                yield return (T)(object) new Record(schema, values, typing == QueryTyping.Inherits ? Connection.QueryTyping : typing);
            }
            yield break;
        }

        List<MemberBinding> GetBindings(Type type, out bool weakBinding)
        {
            var setters = new List<MemberBinding>();

            weakBinding = type.GetCustomAttribute<WeakBinding>() != null;

            var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var fieldInfo in fields)
            {
                if (weakBinding)
                {
                    var bind = fieldInfo.GetCustomAttribute<BindAttribute>();
                    var isIgnore = fieldInfo.GetCustomAttribute<BindIgnoreAttribute>() != null;
                    if (!isIgnore)
                        setters.Add(new MemberBinding(fieldInfo));
                }
                else
                {
                    var attr = fieldInfo.GetCustomAttribute<BindAttribute>();
                    if (attr != null) setters.Add(new MemberBinding(fieldInfo, attr));
                }
            }

            foreach (var propInfo in props)
            {
                if (weakBinding)
                {
                    var bind = propInfo.GetCustomAttribute<BindAttribute>();
                    var isIgnore = propInfo.GetCustomAttribute<BindIgnoreAttribute>() != null;
                    if(!isIgnore)
                        setters.Add(new MemberBinding(propInfo, bind));
                }
                else
                {
                    var attr = propInfo.GetCustomAttribute<BindAttribute>();
                    if (attr != null) setters.Add(new MemberBinding(propInfo, attr));
                }
            }

            return setters;
        }

        IEnumerable<T> ReadStrict(DbDataReader reader, QueryTyping typing)
        {
            var typingParam = typing == QueryTyping.Inherits ? (Connection.QueryTyping == QueryTyping.Inherits ? QueryTyping.Strong : Connection.QueryTyping) : typing;

            var result = new List<T>();

            bool weakBinding;
            var bindings = GetBindings(typeof(T), out weakBinding);
            if (bindings.Count == 0 && !weakBinding) yield break;

            var readerCount = reader.FieldCount;
            var match = new List<Tuple<int, int>>();

            var schSql = reader.GetSchemaTable();
            int dbnullDescId = schSql.Columns.IndexOf("AllowDBNull");

            for (int i = 0; i < bindings.Count; i++)
            {
                bool matchFinded = false;

                var fldName = bindings[i].Name.ToLower();
                var fldType = bindings[i].FieldType;

                for (int j = 0; j < readerCount; j++)
                {
                    var colName = reader.GetName(j).ToLower();
                    var colType = reader.GetFieldType(j);

                    if (fldName == colName)
                    {
                        if(typingParam == QueryTyping.Strong)
                        {
                            var allowNull = (bool)schSql.Rows[j][dbnullDescId];
                            bool nullable = false;
                            if (fldType.IsGenericType && fldType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                fldType = fldType.GetGenericArguments()[0];
                                nullable = true;
                            }

                            if (colType != fldType || (nullable == false && allowNull == true))
                                throw new TypeMatchException(bindings[i].Name);
                        }

                        match.Add(new Tuple<int, int>(i, j));
                        matchFinded = true;
                        break;
                    }
                }

                if (!matchFinded && !weakBinding) throw new BindingException(fldName);
            }

            while (reader.Read())
            {
                object rec = default(T);
                if (rec == null) rec = Activator.CreateInstance<T>();

                foreach (var m in match)
                {
                    var val = reader.GetValue(m.Item2);

                    if (val is DBNull)
                        bindings[m.Item1].Set(rec, null);
                    else
                    {
                        if(typingParam == QueryTyping.Weak)
                            bindings[m.Item1].Set(rec, Convert.ChangeType(val, bindings[m.Item1].FieldType));
                        else
                            bindings[m.Item1].Set(rec, val);
                    }
                }
                yield return (T)rec;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ExecuteEnumerable(QueryTyping.Inherits, null).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class WeakBinding : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class BindIgnoreAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class BindAttribute : Attribute
    {
        string fieldName = null;
        bool isKey = false;

        public string FieldName { get { return fieldName; } }

        public BindAttribute(string fieldName, bool isKey)
        {
            this.fieldName = fieldName;
            this.isKey = isKey;
        }

        public BindAttribute(string fieldName)
        {
            this.fieldName = fieldName;
            this.isKey = false;
        }
    }


    public interface IQuery : IEnumerable { }
    public interface IQuery<T> : IQuery, IEnumerable<T> { }

    public interface IQueryLogger
    {
        void Write(string query);
    }

    public enum QueryTyping
    {
        Weak,
        Strong,
        Inherits
    }

    public class Query : Query<dynamic>
    {
        public Query(DbConnection connection, string query, CommandType type = CommandType.Text, params DbParameter[] args)
            : base(connection, query, type, args)
        {
        }
    }

    public class TypeMatchException : Exception
    {
        readonly string fieldName;

        public override string Message
        {
            get
            {
                return $"Тип поля {fieldName} в целевом объекте отличается от типа поля в результатах запроса. Используйти перечисление QueryTyping для автоматического преобразования.";
            }
        }

        public TypeMatchException(string fieldName)
        {
            this.fieldName = fieldName;
        }
    }

    public class BindingException : Exception
    {
        readonly string fieldName;

        public override string Message
        {
            get
            {
                return $"Поле {fieldName} не найдено в результирующем наборе";
            }
        }

        public BindingException(string fieldName)
        {
            this.fieldName = fieldName;
        }
    }

    partial class Query<T>
    {
        class MemberBinding
        {
            FieldInfo fieldInfo;
            PropertyInfo propInfo;
            string name = null;

            public MemberBinding(FieldInfo fieldInfo, BindAttribute attr = null)
            {
                this.fieldInfo = fieldInfo;
                name = attr == null ? fieldInfo.Name : (attr.FieldName == null ? fieldInfo.Name : attr.FieldName);
            }

            Type GetFieldType(Type type)
            {
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    return type.GetGenericArguments()[0];
                return type;
            }

            public MemberBinding(PropertyInfo propInfo, BindAttribute attr = null)
            {
                this.propInfo = propInfo;
                name = attr == null ? propInfo.Name : (attr.FieldName == null ? propInfo.Name : attr.FieldName);
            }

            public void Set(object instance, object value)
            {
                if (fieldInfo != null) fieldInfo.SetValue(instance, value);
                if (propInfo != null) propInfo.SetValue(instance, value, null);
            }

            public Type FieldType
            {
                get
                {
                    if (fieldInfo != null) return GetFieldType(fieldInfo.FieldType);
                    else if (propInfo != null) return GetFieldType(propInfo.PropertyType);
                    return null;
                }
            }

            public string Name
            {
                get { return name; }
            }
        }
    }

#if DEBUG

    [DebuggerDisplay("{ToString()}")]
    [DebuggerTypeProxy(typeof(Query<int>.DebugProxy))]
    partial class Query<T>
    {
        class DebugProxy
        {
            Query<T> query;
            public DebugProxy(Query<T> query)
            {
                this.query = query;
            }

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public List<T> Display
            {
                get
                {
                    return query.Execute();
                }
            }
        }

        public override string ToString()
        {
            if (query == null) return base.ToString();
            return query;
        }

    }
#endif
}
