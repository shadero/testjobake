﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace TestJob.Db
{
    class ConnectionCache
    {
        Dictionary<string, DbConnection> cache = new Dictionary<string, DbConnection>();
        DbConnection defaultConn;

        public void Set(DbConnection conn, string name)
        {
            if (cache.ContainsKey(name))
                cache[name] = conn;
            else
                cache.Add(name, conn);
        }

        public DbConnection Get(string name)
        {
            if (cache.ContainsKey(name)) return cache[name];
            return null;
        }

        public void MakeDefault(DbConnection conn)
        {
            defaultConn = conn;
        }

        public DbConnection GetDefault()
        {
            return defaultConn;
        }
    }
}
