﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestJob.Db
{
    public partial class Record : IDynamicMetaObjectProvider
    {
        Schema scheme;
        object[] values;
        QueryTyping typing;

        public dynamic this[string name]
        {
            get { return Get(name); }
            set { Set(name, value); }
        }

        public dynamic this[int id]
        {
            get { return values[id]; }
            set { values[id] = value; }
        }

        public Record(Schema scheme, object[] values, QueryTyping typing)
        {
            this.scheme = scheme;
            this.typing = typing;

            if (typing == QueryTyping.Strong || typing == QueryTyping.Inherits)
                this.values = values;
            else
            {
                this.values = values.Select(s => new WeakValue(s)).ToArray();
            }
        }

        public dynamic Get(string name)
        {
            int id = scheme[name.ToLower()].Ordinal;
            return values[id];
        }

        public dynamic Set(string name, dynamic value)
        {
            int id = scheme[name.ToLower()].Ordinal;

            if (typing == QueryTyping.Weak)
            {
                if(value.GetType() == typeof(WeakValue))
                    values[id] = value;
                else
                    values[id] = new WeakValue(value);
            }
            else
                values[id] = value;

            return values[id];
        }

        public override string ToString()
        {
            string ret = "";
            foreach (var ob in values)
                ret += (ret != "" ? " | " : "") + ob.ToString();
            return ret;
        }

        static MethodInfo getMethod = typeof(Record).GetMethod("Get");
        static MethodInfo setMethod = typeof(Record).GetMethod("Set");

        class RecordDMO : DynamicMetaObject
        {
            public RecordDMO(Expression parameter, object value)
                : base(parameter, BindingRestrictions.Empty, value)
            {
            }

            public override DynamicMetaObject BindGetMember(GetMemberBinder binder)
            {
                Expression convExpr = Expression.Convert(Expression, LimitType);
                BindingRestrictions rest = BindingRestrictions.GetTypeRestriction(Expression, LimitType);

                var parameters = new Expression[]
                {
                    Expression.Constant(binder.Name)
                };

                return new DynamicMetaObject(Expression.Call(convExpr, getMethod, parameters), rest);
            }

            public override DynamicMetaObject BindSetMember(SetMemberBinder binder, DynamicMetaObject value)
            {
                Expression convExpr = Expression.Convert(Expression, LimitType);
                BindingRestrictions rest = BindingRestrictions.GetTypeRestriction(Expression, LimitType);

                var parameters = new Expression[]
                {
                    Expression.Constant(binder.Name),
                    Expression.Convert(value.Expression, typeof(object))
                };

                return new DynamicMetaObject(Expression.Call(convExpr, setMethod, parameters), rest);
            }
        }

        DynamicMetaObject IDynamicMetaObjectProvider.GetMetaObject(Expression parameter)
        {
            return new RecordDMO(parameter, this);
        }
    }

#if DEBUG
    [DebuggerTypeProxy(typeof(Record.DebugProxy))]
    partial class Record
    {
        class DebugProxy
        {
            Record record;

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public List<string> Display
            {
                get
                {
                    var ret = new List<string>();
                    foreach (var kpv in record.scheme)
                        ret.Add(kpv.Key + " = " + record.values[kpv.Value.Ordinal]);
                    return ret;
                }
            }

            public DebugProxy(Record record)
            {
                this.record = record;
            }
        }
    }
#endif

}
