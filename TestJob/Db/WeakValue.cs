﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace TestJob.Db
{
    class WeakValue : IDynamicMetaObjectProvider
    {

        class ValueDMO : DynamicMetaObject
        {
            public ValueDMO(Expression parameter, object value)
                : base(parameter, BindingRestrictions.Empty, value)
            { }

            public override DynamicMetaObject BindBinaryOperation(BinaryOperationBinder binder, DynamicMetaObject arg)
            {
                if (binder.Operation == ExpressionType.Equal || binder.Operation == ExpressionType.NotEqual ||
                    binder.Operation == ExpressionType.Multiply || binder.Operation == ExpressionType.Divide ||
                    binder.Operation == ExpressionType.Add || binder.Operation == ExpressionType.Subtract)
                {
                    var parameters = new Expression[]
                    {
                        Expression.Constant(arg.LimitType)
                    };

                    var retr = Expression.Convert(Expression.Call(
                            Expression.Convert(Expression, LimitType),
                            typeof(WeakValue).GetMethod("Get"),
                            parameters), arg.LimitType);

                    var exp = Expression.Convert(Expression.MakeBinary(binder.Operation, retr, arg.Expression), binder.ReturnType);
                    var obj = new DynamicMetaObject(exp, base.BindBinaryOperation(binder, arg).Restrictions);

                    return obj;
                }
                else
                {
                    return base.BindBinaryOperation(binder, arg);
                }
            }

            public override DynamicMetaObject BindUnaryOperation(UnaryOperationBinder binder)
            {
                return base.BindUnaryOperation(binder);
            }

            public override DynamicMetaObject BindConvert(ConvertBinder binder)
            {
                if (binder.ReturnType == typeof(string))
                {
                    var parameters = new Expression[]
                    {
                        Expression.Constant(binder.ReturnType)
                    };

                    var exp = Expression.Call(
                            Expression.Convert(Expression, LimitType),
                            typeof(WeakValue).GetMethod("ToString"));

                    return new DynamicMetaObject(exp, BindingRestrictions.GetTypeRestriction(Expression, LimitType));
                }
                else
                {
                    var parameters = new Expression[]
                    {
                        Expression.Constant(binder.ReturnType)
                    };

                    var exp = Expression.Convert(Expression.Call(
                            Expression.Convert(Expression, LimitType),
                            typeof(WeakValue).GetMethod("Get"),
                            parameters), binder.ReturnType);

                    return new DynamicMetaObject(exp, BindingRestrictions.GetTypeRestriction(Expression, LimitType));
                }
            }
        }

        object value;

        public WeakValue(object value)
        {
            this.value = value;
        }

        DynamicMetaObject IDynamicMetaObjectProvider.GetMetaObject(Expression parameter)
        {
            return new ValueDMO(parameter, this);
        }

        public object Get(Type type)
        {
            if (value is DBNull)
            {
                if (type.IsValueType) return Activator.CreateInstance(type);
                else return null;
            }

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                return Activator.CreateInstance(type, Convert.ChangeType(value, type.GetGenericArguments()[0]));

            return Convert.ChangeType(value, type);
        }

        public override string ToString()
        {
            if (value == null || value is DBNull) return "";
            return value.ToString();
        }
    }
}
