﻿using System;
using System.Data.Common;
using System.Data.OleDb;

namespace TestJob.Db
{
    public static partial class Connection
    {
        static public DbConnection dBASE4(string path)
        {
            var dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=dBASE IV;";
            return dbConnection;
        }
    }
}
