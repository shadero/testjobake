﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;

namespace TestJob.Db
{
    public static partial class Connection
    {
        static public DbConnection MSSql(string connectionString)
        {
            var builder = new SqlConnectionStringBuilder(connectionString);
            //builder.ConnectTimeout = 0;
            builder.ApplicationName = Assembly.GetEntryAssembly().GetName().Name;
            var str = builder.ToString();
            var conn = new SqlConnection(str);
            return conn;
        }

        static public DbConnection MSSql(string dataSource, string catalog, string user, string password)
        {
            var connString = $"Password={password};Persist Security Info=True;User ID={user};Initial Catalog={catalog};Data Source={dataSource}";
            return MSSql(connString);
        }
    }
}
