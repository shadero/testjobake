﻿using System;
using System.Data.Common;
using System.Reflection;
using TestJob.Properties;

namespace TestJob.Db
{
    public static partial class Connection
    {
        static ConstructorInfo MySqlConstructor = null;

        public static DbConnection MySql(string connectionString)
        {
            if (MySqlConstructor == null)
            {
                var assembly = Assembly.Load(Resources.MySql_Data);
                var connType = assembly.GetType("MySql.Data.MySqlClient.MySqlConnection");
                MySqlConstructor = connType.GetConstructor(new Type[] { typeof(string) });
            }

            return MySqlConstructor.Invoke(new object[] { connectionString }) as DbConnection;
        }

        public static DbConnection MySql(string dataSource, string catalog, string user, string password)
        {
            return MySql($@"Password={password};User ID={user};Initial Catalog={catalog};Data Source={dataSource}");
        }
    }
}
