﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestJob.Db
{

#if DEBUG
    [DebuggerDisplay("{ToString()}")]
    [DebuggerTypeProxy(typeof(DebugProxy))]
#endif

    public class Schema : Dictionary<string, Schema.Field>
    {
        public struct Field
        {
            public int Ordinal;
            public Type Type;
            public string Name;

            public override string ToString()
            {
                return Name + " " + Type.Name;
            }
        }

        public override string ToString()
        {
            string ret = "";
            foreach (var ob in Values)
                ret += (ret != "" ? " | " : "") + ob.Name;
            return ret;
        }

#if DEBUG
        class DebugProxy
        {
            Schema scheme;

            public DebugProxy(Schema scheme)
            {
                this.scheme = scheme;
            }

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public List<Schema.Field> View
            {
                get { return scheme.Values.ToList(); }
            }
        }
#endif

    }
}
