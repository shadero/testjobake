﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TestJob.Db
{
    static class Extensions
    {
        public static T GetCustomAttribute<T>(this Type type, bool inherit = true) where T : Attribute
        {
            var attrs = type.GetCustomAttributes(typeof(T), inherit);
            if (attrs.Length < 1) return null;
            return attrs[0] as T;
        }

        public static T GetCustomAttribute<T>(this MemberInfo info, bool inherit = true) where T : Attribute
        {
            var attrs = info.GetCustomAttributes(typeof(T), inherit);
            if (attrs.Length < 1) return null;
            return attrs[0] as T;
        }
    }
}
