﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestJob.Model;
using TestJob.ViewModel;

namespace TestJob.View
{
    /// <summary>
    /// Логика взаимодействия для MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var model = DataModel.Instance;
            var facultyList = model.FacultySet.Local.ToBindingList();
            new FacultyDirectoryView(facultyList).ShowDialog();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            new GroupDirectoryView().ShowDialog();
        }
        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            new StudentsListView().ShowDialog();
        }
        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            new DisciplineListView().ShowDialog();
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            new SemesterListView().ShowDialog();
        }
    }
}
