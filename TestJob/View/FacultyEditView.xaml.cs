﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestJob.Model;

namespace TestJob.View
{
    /// <summary>
    /// Логика взаимодействия для GroupEditView.xaml
    /// </summary>
    public partial class FacultyEditView : Window
    {
        public Faculty Faculty { get; set; }

        public FacultyEditView(Faculty faculty)
        {
            this.Faculty = faculty;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
