﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestJob.Model;
using TestJob.ViewModel;

namespace TestJob.View
{
    /// <summary>
    /// Логика взаимодействия для DisciplineEditView.xaml
    /// </summary>
    public partial class DisciplineEditView : Window
    {
        public DisciplineEditView(StudySubject disp)
        {
            InitializeComponent();
            DataContext = new DisciplineEditViewModel(disp);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
