﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestJob.Model;
using TestJob.ViewModel;

namespace TestJob.View
{
    /// <summary>
    /// Логика взаимодействия для GroupEditView.xaml
    /// </summary>
    public partial class GroupEditView : Window
    {
        public GroupEditView(Group group, IEnumerable<Faculty> faculties)
        {
            InitializeComponent();

            this.DataContext = new GroupEditViewModel { Faculties = faculties, Group = group };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
