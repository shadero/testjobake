﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestJob.Model;

namespace TestJob.View
{
    /// <summary>
    /// Логика взаимодействия для GroupsDirectoryView.xaml
    /// </summary>
    public partial class FacultyDirectoryView : Window
    {
        public BindingList<Faculty> facultyList { get; set; }

        public FacultyDirectoryView(BindingList<Faculty> facultyList)
        {
            this.facultyList = facultyList;

            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var item = new Faculty();

            var frm = new FacultyEditView(item);
            frm.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            if(frm.ShowDialog() == true)
                facultyList.Add(item);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var selected = gridFaculties.SelectedItem as Faculty;
            if(selected != null) facultyList.Remove(selected);
        }
    }
}
