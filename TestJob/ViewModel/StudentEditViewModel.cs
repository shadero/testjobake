﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestJob.Model;

namespace TestJob.ViewModel
{
    class StudentEditViewModel
    {
        public Student Student { get; }
        public IEnumerable<Group> Groups { get; }
        public IEnumerable<StudentStatus> StudentStatuses { get; }

        public Group SelectedGroup
        {
            get
            {
                return Student?.Group;
            }
            set
            {
                if (Student != null)
                {
                    Student.Group = value;
                    Student.GroupId = value?.Id ?? 0;
                }
            }
        }

        public StudentStatus SelectedStatus
        {
            get
            {
                return Student.Status;
            }
            set
            {
                Student.StatusId = (int)value;
            }
        }

        public StudentEditViewModel()
        {
            var model = DataModel.Instance;
            model.GroupSet.Load();
            Groups = model.GroupSet.Local.ToBindingList();

            StudentStatuses = new List<StudentStatus>
            {
                StudentStatus.Active,
                StudentStatus.Academic,
                StudentStatus.Candidate,
                StudentStatus.Expelled,
                StudentStatus.Sick
            };
        }
    }
}
