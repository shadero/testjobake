﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestJob.Model;
using TestJob.View;

namespace TestJob.ViewModel
{
    class SemesterListViewModel : INotifyPropertyChanged
    {
        public BindingList<Semester> Semesters { get; }
        public RelayCommand AddCommand { get; }
        public RelayCommand DeleteCommand { get; }
        public event PropertyChangedEventHandler PropertyChanged;

        public SemesterListViewModel()
        {
            var model = DataModel.Instance;

            model.SemesterSet.Load();

            Semesters = model.SemesterSet.Local.ToBindingList();

            AddCommand = new RelayCommand(() =>
            {
                var item = new Semester();
                var frm = new SemesterEditView(item);

                frm.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                if (frm.ShowDialog() == true)
                {
                    Semesters.Add(item);
                    model.SaveChanges();

                    PropertyChanged(this, new PropertyChangedEventArgs("Semesters"));
                }
            });

        }
    }
}
