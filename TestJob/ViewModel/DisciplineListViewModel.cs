﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestJob.Model;
using TestJob.View;

namespace TestJob.ViewModel
{
    class DisciplineListViewModel : INotifyPropertyChanged
    {
        public BindingList<StudySubject> StudySubjects { get; }
        public RelayCommand AddCommand { get; }
        public RelayCommand DeleteCommand { get; }
        public event PropertyChangedEventHandler PropertyChanged;

        public DisciplineListViewModel()
        {
            var model = DataModel.Instance;

            model.StudySubjectSet.Load();

            StudySubjects = model.StudySubjectSet.Local.ToBindingList();

            AddCommand = new RelayCommand(() =>
            {
                var item = new StudySubject();
                var frm = new DisciplineEditView(item);

                frm.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                if (frm.ShowDialog() == true)
                {
                    StudySubjects.Add(item);
                    model.SaveChanges();

                    PropertyChanged(this, new PropertyChangedEventArgs("StudySubjects"));
                }
            });

        }
    }
}
