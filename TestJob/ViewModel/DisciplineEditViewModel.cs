﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestJob.Model;

namespace TestJob.ViewModel
{
    public class DisciplineEditViewModel
    {
        public StudySubject StudySubject { get; }

        public DisciplineEditViewModel()
        {

        }
        public DisciplineEditViewModel(StudySubject disp)
        {
            StudySubject = disp;
        }
    }
}
