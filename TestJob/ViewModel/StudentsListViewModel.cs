﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestJob.Model;

namespace TestJob.ViewModel
{
    class StudentsListViewModel
    {
        public BindingList<Student> Students { get; set; }
    }
}
