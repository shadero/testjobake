﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestJob.Model;

namespace TestJob.ViewModel
{
    class GroupEditViewModel : INotifyPropertyChanged
    {
        public IEnumerable<Faculty> Faculties { get; set; }
        public Group Group { get; set; }

        public Faculty SelectedFaculty
        {
            get { return Group?.Faculty; }

            set
            {
                if (Group == null) return;

                Group.FacultyId = value?.Id ?? 0;
                Group.Faculty = value;
            }
        }

        public string Name
        {
            get { return Group?.Name; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public RelayCommand UpdateNameCommand { get; }

        public GroupEditViewModel()
        {
            UpdateNameCommand = new RelayCommand((s) =>
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Name"));
            });
        }
    }

}
