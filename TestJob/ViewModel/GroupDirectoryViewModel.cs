﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestJob.Model;
using TestJob.View;

namespace TestJob.ViewModel
{
    class GroupDirectoryViewModel : INotifyPropertyChanged
    {
        public BindingList<Group> Groups { get; }
        public IEnumerable<Faculty> Faculties { get; }
        public RelayCommand AddCommand { get; }
        public RelayCommand DeleteCommand { get; }
        public Group SelectedGroup { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public GroupDirectoryViewModel()
        {
            var model = DataModel.Instance;

            model.GroupSet.Load();
            model.FacultySet.Load();

            Groups = model.GroupSet.Local.ToBindingList();
            Faculties = model.FacultySet.Local.ToList();

            AddCommand = new RelayCommand(() =>
            {
                var item = new Group();
                var frm = new GroupEditView(item, Faculties);

                frm.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                if (frm.ShowDialog() == true)
                {
                    Groups.Add(item);
                    model.SaveChanges();

                    PropertyChanged(this, new PropertyChangedEventArgs("Groups"));
                }
            });

            DeleteCommand = new RelayCommand(() =>
            {
                if (SelectedGroup != null)
                {
                    Groups.Remove(SelectedGroup);
                    model.SaveChanges();

                    PropertyChanged(this, new PropertyChangedEventArgs("Groups"));
                }
            });
        }

    }

}
