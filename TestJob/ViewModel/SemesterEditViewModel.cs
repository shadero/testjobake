﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestJob.Model;

namespace TestJob.ViewModel
{
    class SemesterEditViewModel
    {
        public Semester Semester { get; set; }
    }
}
